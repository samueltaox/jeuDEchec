// ----------------------------------------------
// Informatique Graphique 3D & Réalité Virtuelle.
// Travaux Pratiques
// Shaders
// Copyright (C) 2015 Tamy Boubekeur
// All rights reserved.
// ----------------------------------------------

// Add here all the value you need to describe the light or the material.
// At first used const values.
// Then, use uniform variables and set them from the CPU program.

/*type of model;
 *0 means board
 *1 means piece either white or black
 */
uniform int type;
uniform float colorID;
uniform sampler2D texture;

varying vec4 P; // fragment-wise position
varying vec3 N; // fragment-wise normal

void main (void) {
    gl_FragColor = vec4 (0.0, 0.0, 0.0, 1.0);

    if(type==0){
      gl_FragColor = texture2D(texture, vec2((P[0]+0.71)/1.42,(P[2]+0.71)/1.42));
    }
    else{
      gl_FragColor = vec4 (colorID/255.0, 0.0, 0.0, 0.0);
    }
}
