// ----------------------------------------------
// Informatique Graphique 3D & Réalité Virtuelle.
// Travaux Pratiques
// Shaders
// Copyright (C) 2015 Tamy Boubekeur
// All rights reserved.
// ----------------------------------------------

// Add here all the value you need to describe the light or the material.
// At first used const values.
// Then, use uniform variables and set them from the CPU program.

/*
 *  type of model,
 *  1 means white piece
 *  2 means black piece
 *  0 means chess board
 */
uniform int type;

/*
 *  Whether a piece is selected or not
 *  1 if the piece is not selected
 *  0 if the piece is selected
 */
uniform float selectedID;
uniform sampler2D boardtexture;
uniform sampler2D woodtexture;

const vec3 lightPos = vec3 (5.0, 5.0, 5.0);
const vec3 specColor = vec3(1.0, 1.0, 1.0);

varying vec4 P; // fragment-wise position
varying vec3 N; // fragment-wise normal


void main (void) {
  vec3 diffuseColor = vec3(0.9, 0.9, selectedID*0.8);
  if(type == 2) diffuseColor = vec3(0.1,0.1,0.1/selectedID);
  gl_FragColor = vec4 (0.0, 0.0, 0.0, 1.0);

  vec3 p = vec3 (gl_ModelViewMatrix * P);
  vec3 n = normalize (gl_NormalMatrix * N);
  vec3 l = normalize (lightPos - p);
  vec3 v = normalize (-p);

  float lambertian = max(dot(l,n),0.0);
  float spec = 0.0;

  if(lambertian > 0.0){
    vec3 halfDir = normalize(l+v);
    float specAngle = max(dot(halfDir,n),0.0);
    spec = pow(specAngle,16.0);
  }

  vec4 lighting = vec4(lambertian*diffuseColor+spec*specColor,1.0);
  vec4 texture1 = texture2D(boardtexture, vec2((P[0]+0.71)/1.42,(P[2]+0.71)/1.42));
  vec4 texture2 = texture2D(woodtexture, vec2((P[0]+0.71)/1.42,(P[2]+0.71)/1.42));

  if(type == 0){
    vec4 texturefin = mix(texture1, texture2, 0.5);
    gl_FragColor = texturefin * lighting;
  }
  else{
    gl_FragColor = lighting * texture2;
  }
}
