// ----------------------------------------------
// Informatique Graphique 3D & R�alit� Virtuelle.
// Travaux Pratiques
// Shaders
// Copyright (C) 2015 Tamy Boubekeur
// All rights reserved.
// ----------------------------------------------
#include <GL/glew.h>
#include <GL/glut.h>
#include <iostream>
#include <vector>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cmath>

#include "Vec3.h"
#include "Camera.h"
#include "Mesh.h"
#include "GLProgram.h"
#include "Piece.h"
#include "Texture.h"
using namespace std;

static const unsigned int DEFAULT_SCREENWIDTH = 1024;
static const unsigned int DEFAULT_SCREENHEIGHT = 768;
static const string DEFAULT_MESH_FILE ("models/man.off");
static int menuIdentifier;

// my own paramaters
static const int NUM_OF_PION = 32;
Board* board;

/*
 * 0~15 are white, 16~ 31 are black
 * 0,7 are rook
 * 1,6 are knight
 * 2,5 are bishop
 * 3 is king
 * 4 is queen
 * 8~15 are pawn
 * 16~23 are pawn
 * 24,31 are rook
 * 25,30 are knight
 * 26,29 are bishop
 * 27 is king
 * 28 is queen
 */
Piece *pieces[NUM_OF_PION];

/*
 * clicked represents the number of times the mouse is clicked.
 * With 3 values, 0, 1, 2 and after that we go back to 0
 * 0 represents no click, we won't use the picking shader
 * 1 represents first click to choose a piece
 * 1 represents first click to choose another piece
 */
static int clicked = 0;
static bool clicked1 = false;   //check whether the first click has been done
static int clickX1, clickY1, clickX2, clickY2, X1, Y1, X2, Y2;
static float width=12;
static float height=12;
GLuint textures[3];
Texture ImageOne;

static string appTitle ("Informatique Graphique & Realite Virtuelle - Travaux Pratiques - Shaders");
static GLint window;
static GLint menuwindow;
static unsigned int FPS = 0;
static bool fullScreen = false;

static Camera camera;
static Mesh mesh[NUM_OF_PION];
Program * lightingMaterial;
Program * pickingMaterial;

int movingPiece = 0;   //id of the piece that is moving

void printUsage () {
  std::cerr << std::endl
	    << appTitle << std::endl
	    << "Author: Tamy Boubekeur" << std::endl << std::endl
	    << "Usage: ./main [<file.off>]" << std::endl
	    << "Commands:" << std::endl
	    << "------------------" << std::endl
	    << " ?: Print help" << std::endl
	    << " w: Toggle wireframe mode" << std::endl
	    << " <drag>+<left button>: rotate model" << std::endl
	    << " <drag>+<right button>: move model" << std::endl
	    << " <drag>+<middle button>: zoom" << std::endl
	    << " q, <esc>: Quit" << std::endl << std::endl;
}

/*Generates the checkerboard on which the game is played.
 *image will be later used to create a texture.
 */
void genCheckerboard (unsigned int width, unsigned int height, unsigned char * image){
  for(unsigned char i=0; i<width;i++){
    for(unsigned char j=0; j<height;j++){
      if(i<2 || i>=width-2){
        image[(i*height+j)*3+0]=255;
        image[(i*height+j)*3+1]=255;
        image[(i*height+j)*3+2]=255;
      }
      else if(j<2 || j>=height-2){
        image[(i*height+j)*3+0]=255;
        image[(i*height+j)*3+1]=255;
        image[(i*height+j)*3+2]=255;
      }
      else{
        if((i+j)%2==0){
          image[(i*height+j)*3+0]=255;
          image[(i*height+j)*3+1]=255;
          image[(i*height+j)*3+2]=255;
        }
        if((i+j)%2==1){
          image[(i*height+j)*3+0]=0;
          image[(i*height+j)*3+1]=0;
          image[(i*height+j)*3+2]=0;
        }
      }
    }
  }
}

/*Generates a 12*12 board with a different id for each case.
 *This will be used to identify the selected case on the board.
 */
void genColorboard (unsigned int width, unsigned int height, unsigned char * image){
  for(unsigned char i=0; i<width; i++){
    for(unsigned char j=0; j<height; j++){
      if(i<2 || i>=width-2){
        image[(i*height+j)*3+0]=33 + i*height + j;
        image[(i*height+j)*3+1]=0;
        image[(i*height+j)*3+2]=0;
      }
      else if(j<2 || j>=height-2){
        image[(i*height+j)*3+0]=33 + i*height + j;
        image[(i*height+j)*3+1]=0;
        image[(i*height+j)*3+2]=0;
      }
      else{
        if((i+j)%2==0){
          image[(i*height+j)*3+0]=33 + i*height + j;
          image[(i*height+j)*3+1]=0;
          image[(i*height+j)*3+2]=0;
        }
        if((i+j)%2==1){
          image[(i*height+j)*3+0]=33 + i*height + j;
          image[(i*height+j)*3+1]=0;
          image[(i*height+j)*3+2]=0;
        }
      }
    }
  }
}

/*Creates the two programs needed.
 *Creates the three textures.
 *Initializes the board and the pieces.
 */
void init () {
    glewInit();
    glCullFace (GL_BACK);     // Specifies the faces to cull (here the ones pointing away from the camera)
    glEnable (GL_CULL_FACE); // Enables face culling (based on the orientation defined by the CW/CCW enumeration).
    glDepthFunc (GL_LESS); // Specify the depth test for the z-buffer
    glEnable (GL_DEPTH_TEST); // Enable the z-buffer in the rasterization


    glLineWidth (2.0); // Set the width of edges in GL_LINE polygon mode
    glClearColor (0.0f, 0.0f, 0.0f, 1.0f); // Background color
    glClearColor (0.0f, 0.0f, 0.0f, 1.0f);

    try {
        pickingMaterial = Program::genVFProgram ("Simple GL Program", "shader.vert", "pickingShader.frag"); // Load and compile pair of shaders
        lightingMaterial = Program::genVFProgram ("Simple GL Program", "shader.vert", "lightingShader.frag");
    }catch (Exception & e) {
        cerr << e.msg () << endl;
    }

    //begin texture setting
    glEnable (GL_TEXTURE_2D); // Activation de la texturation 2D
    glGenTextures (3, textures); // G�n�ration d?une texture OpenGL


    //First texture: CheckerBoard
    glActiveTexture(GL_TEXTURE0);
    glBindTexture (GL_TEXTURE_2D, textures[0]); // Activation de la texture comme texture courante
    // les 4 lignes suivantes param�tre le filtrage de texture ainsi que sa r�p�tition au-del� du carr�
    //unitaire
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    //La commande suivante remplit la texture (sur GPU) avec les donn�es de l?image
    int length=3*width*height;
    unsigned char *image = new unsigned char[length];
    genCheckerboard(width, height, image);
    glTexImage2D (GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);

    //Second texture: ColorBoard
    glActiveTexture(GL_TEXTURE1);
    glBindTexture (GL_TEXTURE_2D, textures[1]);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    genColorboard(width, height, image);
    glTexImage2D (GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);

    //Third texture: wood
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, textures[2]);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    ImageOne.ReadPPMImage("data/wood-slider-background.ppm");
    Image wood = ImageOne.image;
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, wood.width,
                    wood.height, 0, GL_RGB, GL_UNSIGNED_BYTE,
                    wood.pixels);

    camera.resize (DEFAULT_SCREENWIDTH, DEFAULT_SCREENHEIGHT); // Setup the camera

    camera.rotationInit(512,300);

    board = new Board();
    pieces[0] = new Rook(1,1,1,board);
    pieces[1] = new Knight(2,1,1,board);
    pieces[2] = new Bishop(3,1,1,board);
    pieces[3] = new Queen(5,1,1,board);
    pieces[4] = new King(4,1,1,board);
    pieces[5] = new Bishop(6,1,1,board);
    pieces[6] = new Knight(7,1,1,board);
    pieces[7] = new Rook(8,1,1,board);
    for(int i = 8;i <16;i ++)
    {
        pieces[i] = new Pawn(i - 7, 2, 1,board);
    }


    for(int i = 16;i <24;i ++)
    {
        pieces[i] = new Pawn(i - 15, 7, 2,board);
    }
    pieces[24] = new Rook(1,8,2,board);
    pieces[25] = new Knight(2,8,2,board);
    pieces[26] = new Bishop(3,8,2,board);
    pieces[27] = new Queen(5,8,2,board);
    pieces[28] = new King(4,8,2,board);
    pieces[29] = new Bishop(6,8,2,board);
    pieces[30] = new Knight(7,8,2,board);
    pieces[31] = new Rook(8,8,2,board);
}

/*This function is called to draw a piece or the board.
 *Its main purpose is making the function drawScene smaller.
 *k is the id of the piece and cat just checks whether it's the board or a piece.
 */
void drawPieces (unsigned int k, unsigned int cat){
    glMatrixMode (GL_MODELVIEW);
    glPushMatrix ();

    if(cat==1)
    {
        glTranslatef(0, -0.1, 0);
        for (unsigned int i = 0; i < board->mesh.T.size (); i++)
        {
            glBegin (GL_TRIANGLES);
            for (unsigned int j = 0; j < 3; j++)
            {
                const Vertex & v = board->mesh.V[board->mesh.T[i].v[j]];
                glNormal3f (v.n[0], v.n[1], v.n[2]); // Specifies current normal vertex
                glVertex3f (v.p[0], v.p[1], v.p[2]); // Emit a vertex (one triangle is emitted each time 3 vertices are emitted)
            }
            glEnd ();
        }
    }
    else
    {
        glTranslatef(pieces[k]->x_ord,pieces[k]->y_ord,pieces[k]->z_ord);
        for (unsigned int i = 0; i < pieces[k]->mesh.T.size (); i++)
        {
            glBegin (GL_TRIANGLES);
            for (unsigned int j = 0; j < 3; j++){
                const Vertex & v = pieces[k]->mesh.V[pieces[k]->mesh.T[i].v[j]];
                glNormal3f (v.n[0], v.n[1], v.n[2]); // Specifies current normal vertex
                glVertex3f (v.p[0], v.p[1], v.p[2]); // Emit a vertex (one triangle is emitted each time 3 vertices are emitted)
            }
            glEnd ();
        }
    }
    glPopMatrix();
}

/*This function returns the coordinates of the case that we clicked on.
 *For example, it returns 1,1 if we click on the first case in the bottom right.
 *This function lacks logic but it does its job.
 */
void getPara(int& x, int &y, int id)
{
    if(id <= 150 && id >=143)
    {
        y = 1;
        x = (151 - id);
    }
    else if(id <= 138 && id >=131)
    {
        y = 2;
        x = (139 - id);
    }
    else if(id <= 126 && id >=119)
    {
        y = 3;
        x = (127 - id);
    }
    else if(id <= 114 && id >=107)
    {
        y = 4;
        x = (115 - id);
    }
    else if(id <= 102 && id >=95)
    {
        y = 5;
        x = (103 - id);
    }
    else if(id <= 90 && id >=83)
    {
        y = 6;
        x = (91 - id);
    }
    else if(id <= 78 && id >=71)
    {
        y = 7;
        x = (79 - id);
    }
    else if(id <= 66 && id >=59)
    {
        y = 8;
        x = (67 - id);
    }
    else
    {
        y = 0;
        x = 0;
    }
}

/*This function draws the board and the pieces twice.
 *The first time, it colors everything with a different id,
 *and reads the clicked pixel information.
 *This first draw is not shown to the user.
 *The second time, it does the normal coloring of the set, with the appropriate animation.
 */
void drawScene () {
  float res;
  if(clicked != 0){
    //Pass 1
    pickingMaterial->use();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    pickingMaterial->setUniform1i("texture", 1);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture (GL_TEXTURE_2D, textures[1]);

    pickingMaterial->setUniform1i("type", 0);
    drawPieces(0, 1);

    for (unsigned int k=0; k<32; k++){
      if(!pieces[k]->isAlive) continue;
      pickingMaterial->setUniform1f("colorID", k+1);
      pickingMaterial->setUniform1i("type", pieces[k]->type);
      drawPieces(k, 0);
    }
    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
    if(clicked == 1)
    {
        glReadPixels(clickX1, viewport[3]-clickY1, 1, 1, GL_RED, GL_FLOAT, &res);
        int temp = (int) (res*255.0);
        movingPiece = temp;
        if(temp == 0)
        {
            clicked = 0;
            clicked1 = false;
        }
        else if(temp < 33){
            X1 = pieces[temp - 1]->x;
            Y1 = pieces[temp - 1]->y;
        }
        else
        {
            clicked = 0;
            clicked1 = false;
        }
    }
    if(clicked == 2)
    {
        glReadPixels(clickX2, viewport[3]-clickY2, 1, 1, GL_RED, GL_FLOAT, &res);
        int temp = (int) (res*255.0);
        if(temp == 0)
        {
            clicked = 0;
            clicked1 = false;
        }
        else if(temp < 33){
            X2 = pieces[temp - 1]->x;
            Y2 = pieces[temp - 1]->y;
        }
        else
        {
            getPara(X2, Y2, temp);
            if(X2 == 0)
            {
                clicked = 0;
                clicked1 = false;
            }
        }
        if(clicked == 2)
        {
            if(board->state[X1 - 1][Y1 - 1]->moveable(X2, Y2))
            {
                board->state[X1 - 1][Y1 - 1]->startMove(X2,Y2);
                clicked = 0;
                clicked1 = false;
            }
        }
    }
  }
  //Pass 2
  lightingMaterial->use();
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  lightingMaterial->setUniform1i("boardtexture", 0);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture (GL_TEXTURE_2D, textures[0]);

  lightingMaterial->setUniform1i("woodtexture", 2);
  glActiveTexture(GL_TEXTURE2);
  glBindTexture (GL_TEXTURE_2D, textures[2]);

  lightingMaterial->setUniform1i("type", 0);
  drawPieces(0, 1);
  for (unsigned int k=0; k<32; k++){
      if(((unsigned int)movingPiece == k +1 )&& pieces[k]->isMoving == true)
          pieces[k]->move();

      if(!pieces[k]->isAlive) continue;
      lightingMaterial->setUniform1i("type", pieces[k]->type);
      if ((k + 1) == (unsigned int)(res*255.0)) lightingMaterial->setUniform1f("selectedID", 0.1);
      else lightingMaterial->setUniform1f("selectedID", 1.0);
      drawPieces(k, 0);
  }
}

void reshape(int w, int h) {
  camera.resize (w, h);
}

void display () {
  //glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  camera.apply ();
  drawScene ();
  glFlush ();
  glutSwapBuffers ();
}

void key (unsigned char keyPressed, int x, int y) {
  switch (keyPressed) {
  case 'f':
    if (fullScreen) {
      glutReshapeWindow (camera.getScreenWidth (), camera.getScreenHeight ());
      fullScreen = false;
    } else {
      glutFullScreen ();
      fullScreen = true;
    }
    break;
  case 'q':
    glutDestroyWindow(window);
    glutSetWindow(menuwindow);
    break;
  case 27:
    exit (0);
    break;
  case 'w':
    GLint mode[2];
    glGetIntegerv (GL_POLYGON_MODE, mode);
    glPolygonMode (GL_FRONT_AND_BACK, mode[1] ==  GL_FILL ? GL_LINE : GL_FILL);
    break;
    break;
  default:
    printUsage ();
    break;
  }
}

/*This function counts the number of clicks.
 *It saves the coordinates of the first two clicks.
 */
void mouse (int button, int state, int x, int y) {
  camera.handleMouseClickEvent (button, state, x, y);
  if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN){
    clicked = (clicked + 1) % 3;
    if(clicked != 0)
    {
        if (clicked1 == false)
        {
            clickX1 = x;
            clickY1 = y;
            clicked1 = true;
        }
        else
        {
            clickX2 = x;
            clickY2 = y;
            clicked1 = false;
        }
    }
  }
}

void motion (int x, int y) {
  camera.handleMouseMoveEvent (x, y);
}

void idle () {
  static float lastTime = glutGet ((GLenum)GLUT_ELAPSED_TIME);
  static unsigned int counter = 0;
  counter++;
  float currentTime = glutGet ((GLenum)GLUT_ELAPSED_TIME);
  if (currentTime - lastTime >= 1000.0f) {
    FPS = counter;
    counter = 0;
    static char winTitle [128];
    unsigned int numOfTriangles = mesh[1].T.size ();
    sprintf (winTitle, "Number Of Triangles: %d - FPS: %d", numOfTriangles, FPS);
    glutSetWindowTitle (winTitle);
    lastTime = currentTime;
  }
  glutPostRedisplay ();
}

void displayMenu(void){
  glClearColor(1.0f, 1.0f, 1.0f, 1.0f); // Clear the background of our window to red
  glClear(GL_COLOR_BUFFER_BIT); //Clear the colour buffer (more buffers later on)
  glLoadIdentity(); // Load the Identity Matrix to reset our drawing locations

  glFlush(); // Flush the OpenGL buffers to the window
}

void play(){
  glutInitDisplayMode (GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
  glutInitWindowSize (DEFAULT_SCREENWIDTH, DEFAULT_SCREENHEIGHT);
  window = glutCreateWindow (appTitle.c_str ());
  init ();
  glutIdleFunc (idle);
  glutReshapeFunc (reshape);
  glutDisplayFunc (display);
  glutKeyboardFunc (key);
  glutMotionFunc (motion);
  glutMouseFunc (mouse);
  printUsage ();
}

void menu(int op) {
  switch(op) {
  case 'Q':
  case 'q':
    exit(0);
  case 'p':
    play();
    break;
  glutPostRedisplay();
  }
}

int main (int argc, char ** argv) {
  if (argc > 1) {
    printUsage ();
    exit (1);
  }
  /*glutInit (&argc, argv);
  glutInitDisplayMode (GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
  glutInitWindowSize (DEFAULT_SCREENWIDTH, DEFAULT_SCREENHEIGHT);
  window = glutCreateWindow (appTitle.c_str ());
  init ();
  glutIdleFunc (idle);
  glutReshapeFunc (reshape);
  glutDisplayFunc (display);
  glutKeyboardFunc (key);
  glutMotionFunc (motion);
  glutMouseFunc (mouse);
  printUsage ();
  glutMainLoop ();
  return 0;*/

  glutInit (&argc, argv);
  glutInitWindowSize (DEFAULT_SCREENWIDTH, DEFAULT_SCREENHEIGHT);
  glutInitWindowPosition (200, 150);
  glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
  menuwindow = glutCreateWindow ("Click anywhere to show menu");

  //glutIdleFunc (idle);
  glutDisplayFunc (displayMenu);
  //glutMouseFunc (mouse);
  //glutReshapeFunc (reshape);

  // create main "left click" menu
  menuIdentifier = glutCreateMenu(menu);
  glutAddMenuEntry("Start a new game", 'p');
  glutAddMenuEntry("Quit", 'q');
  glutAttachMenu(GLUT_LEFT_BUTTON);
  glutMainLoop();
  return 0;
}
