#pragma once
#include "Mesh.h"
#include <iostream>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <cmath>

using namespace std;

class Board;
class Piece
{
public:
    //coordinates for board 8*8
    int x;
    int y;
    Board *board;
    //coordinates for 3D
    float xf;
    float yf;
    float x_ord;
    float y_ord;
    float z_ord;

    bool isMoving;
    float X2, Y2;
    int timeForOneMove;
    int currentMoveTime;

    // 1 means white side
    // 2 means black side
    int type;

    bool isAlive;
    string adress;
    Mesh mesh;

    Piece(int x_, int y_, int side_, Board*);
    //test if the new position is moveable
    virtual bool moveable(int newX, int newY);
    virtual void move();
    virtual void setBoard(Board*);
    virtual void startMove(int newX, int newY);
};

class King:public Piece
{
public:

    King(int x_, int y_, int side_,Board*);

    virtual bool moveable(int newX, int newY);
    //virtual void move(int newX, int newY);
    //bool isDead();
};

class Queen: public Piece
{

  public:
  Queen(int x_, int y_, int side_,Board*);

  virtual bool moveable(int newX, int newY);
  //virtual void move(int newX, int newY);
};

class Rook: public Piece
{
public:
  Rook(int x_, int y_, int side_,Board*);

    virtual bool moveable(int newX, int newY);
    //virtual void move(int newX, int newY);
};

class Bishop:public Piece
{
public:
  Bishop(int x_, int y_, int side_,Board*);

  virtual bool moveable(int newX, int newY);
  //virtual void move(int newX, int newY);
};

class Knight: public Piece
{
public:
  Knight(int x_, int y_, int side_,Board*);

  virtual bool moveable(int newX, int newY);
  //virtual void move(int newX, int newY);
};

class Pawn:public Piece
{
public:
  Pawn(int x_, int y_, int side_,Board*);

  virtual bool moveable(int newX, int newY);
  //virtual void move(int newX, int newY);
};

class Board
{
public:
    string adress;
    Mesh mesh;

    Piece* state[8][8];
    inline Board(){
        adress = "models/plane_simple.off";
        mesh.loadOFF(adress, 1);
        for(int i = 0;i < 8;i ++)
        {
            for(int j = 0;j < 8;j ++)
            {
                state[i][j] = NULL;
            }
        }
    };

    void setPiece(int x, int y,Piece *);
};
