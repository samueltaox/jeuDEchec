#include "Piece.h"

Piece::Piece(int x_, int y_, int side_, Board *b)
{
  x = x_;
  y = y_;
  //if()
  x_ord = 0.54 - 0.12*x;
  y_ord = 0;
  z_ord = 0.54 - 0.12*y;
  type = side_;
  board = b;
  board->setPiece(x,y,this);
  isAlive = true;
  isMoving = false;
  X2 = -1;
  Y2 = -1;
  timeForOneMove = 50;
  currentMoveTime = 0;

};

bool Piece::moveable(int x2, int y2)
{

    return true;
}

void Piece::startMove(int newX, int newY)
{
    X2 = newX - x;
    Y2 = newY - y;
    xf = (float)x;
    yf = (float)y;
    isMoving = true;
    currentMoveTime = 0;

    board->state[(int)(x - 1)][(int)(y - 1)] = NULL;
    Piece *p = board->state[newX - 1][newY - 1];
    board->state[newX - 1][newY - 1] = this;

    x = newX;
    y = newY;

    if(p != NULL)
        p->isAlive = false;

}

void Piece::move()
{
    if(currentMoveTime < timeForOneMove)
    {
        xf += X2/timeForOneMove;
        yf += Y2/timeForOneMove;
        x_ord = 0.54 - 0.12*xf;
        y_ord = 0;
        z_ord = 0.54 - 0.12*yf;
        currentMoveTime ++;
    }
    if(currentMoveTime == timeForOneMove)
    {
        currentMoveTime = 0;
        isMoving = false;
    }

}

void Piece::setBoard(Board * board_)
{
    board = board_;
}

King::King(int x_, int y_, int side_,Board *b):
    Piece(x_,y_,side_,b)
{
    adress = "models/king.off";
    mesh.loadOFF(adress, 0.10);
};


bool King::moveable(int newX, int newY)
{
    if((newX == x) && (newY == y))
        return false;
    else if(board->state[newX -1][newY - 1] != NULL && type == board->state[newX -1][newY - 1]->type)
        return false;
    else if((newX - x) <= 1 && (newX - x) >= -1 && (newY - y) <= 1 && (newY - y)>= -1)
        return true;
    else return false;
};

Queen::Queen(int x_, int y_, int side_, Board *b):
    Piece(x_,y_,side_,b)
{
    adress = "models/queen.off";
    mesh.loadOFF(adress, 0.10);
}
;
bool Queen::moveable(int newX, int newY)
{
  //test if "same position"
    if((newX == x) && (newY == y))
        return false;
    //test if "same side(white or black)"
    else if(board->state[newX -1][newY - 1] != NULL && type == board->state[newX -1][newY - 1]->type)
        return false;

    // test whether there is any piece between old and new position
    //condition same line or colom
    else if((newX - x) == 0 || (newY - y) == 0)
    {
        if(newX == x)
        {
            if(newY < y)
              for(int i = newY; i < y -1;i ++ )
              {
                  if(board->state[newX-1][i] != NULL)
                      return false;
              }
            else
              for(int i = y; i < newY -1;i ++ )
              {
                  if(board->state[newX-1][i] != NULL)
                      return false;
              }
        }
        else if(newY == y)
        {
            if(newX < x)
              for(int i = newX; i < x -1;i ++ )
              {
                  if(board->state[i][newY - 1] != NULL)
                      return false;
              }
            else
              for(int i = x; i < newX -1;i ++ )
              {
                  if(board->state[i][newY - 1] != NULL)
                      return false;
              }
        }
        return true;
    }

    //condition dans slash
    else if ((newX - x) == (newY - y) || (newX - x) == -(newY - y))
    {
        if((newX - x) == (newY - y))
        {
            if(newX < x)
                for(int i = 1; i < x - newX ;i ++ )
                {
                    if(board->state[newX -1 + i][newY -1 + i] != NULL)
                        return false;
                }
            else
                for(int i = 1; i < newX -x;i ++ )
                {
                    if(board->state[x -1 + i][y -1 + i] != NULL)
                        return false;
                }
                return true;
        }
        else
        {
            if(newX < x)
                for(int i = 1; i < x - newX ;i ++ )
                {
                    if(board->state[newX -1 + i][newY -1 - i] != NULL)
                        return false;
                }
            else
                for(int i = 1; i < newX -x;i ++ )
                {
                    if(board->state[newX -1 - i][newY -1 + i] != NULL)
                        return false;
                }
            return true;
        }
    }
    return false;
};

Knight::Knight(int x_, int y_, int side_, Board *b):
    Piece(x_,y_,side_,b)
{
    adress = "models/knight.off";
    mesh.loadOFF(adress, 0.10);
};

bool Knight::moveable(int newX, int newY)
{
    if((newX == x) && (newY == y))
        return false;
    else if(board->state[newX -1][newY - 1] != NULL && type == board->state[newX -1][newY - 1]->type)
        return false;
    else if(((newX - x) == 1 || (newX - x) == -1)&& ((newY - y) == 2 || (newY - y) == -2))
        return true;
    else if(((newX - x) == 2 || (newX - x) == -2)&& ((newY - y) == 1 || (newY - y) == -1))
        return true;
    else return false;
};

Rook::Rook(int x_, int y_, int side_, Board *b):
    Piece(x_,y_,side_,b)
{
    adress = "models/rook.off";
    mesh.loadOFF(adress, 0.10);
};

bool Rook::moveable(int newX, int newY)
{
    if((newX == x) && (newY == y))
        return false;
    else if(board->state[newX -1][newY - 1] != NULL && type == board->state[newX -1][newY - 1]->type)
        return false;

    // test whether there is any piece between old and new position
    //condition same line or colom
    else if((newX - x) == 0 || (newY - y) == 0)
    {
        if(newX == x)
        {
            if(newY < y)
              for(int i = newY; i < y -1;i ++ )
              {
                  if(board->state[newX-1][i] != NULL)
                      return false;
              }
            else
              for(int i = y; i < newY -1;i ++ )
              {
                  if(board->state[newX-1][i] != NULL)
                      return false;
              }
        }
        if(newY == y)
        {
            if(newX < x)
              for(int i = newX; i < x -1;i ++ )
              {
                  if(board->state[i][newY - 1] != NULL)
                      return false;
              }
            else
              for(int i = x; i < newX -1;i ++ )
              {
                  if(board->state[i][newY - 1] != NULL)
                      return false;
              }
        }
        return true;
    }
    return false;
};


Bishop::Bishop(int x_, int y_, int side_, Board *b):
    Piece(x_,y_,side_,b)
{
    adress = "models/bishop.off";
    mesh.loadOFF(adress, 0.10);
};

bool Bishop::moveable(int newX, int newY)
{
    if((newX == x) && (newY == y))
        return false;
    else if(board->state[newX -1][newY - 1] != NULL && type == board->state[newX -1][newY - 1]->type)
        return false;
    //condition dans slash
    else if ((newX - x) == (newY - y) || (newX - x) == -(newY - y))
    {
        if((newX - x) == (newY - y))
        {
            if(newX < x)
                for(int i = 1; i < x - newX ;i ++ )
                {
                    if(board->state[newX -1 + i][newY -1 + i] != NULL)
                        return false;
                }
            else
                for(int i = 1; i < newX -x;i ++ )
                {
                    if(board->state[x -1 + i][y -1 + i] != NULL)
                        return false;
                }
            return true;
        }
        else
        {
            if(newX < x)
                for(int i = 1; i < x - newX ;i ++ )
                {
                    if(board->state[newX -1 + i][newY -1 - i] != NULL)
                        return false;
                }
            else
                for(int i = 1; i < newX -x;i ++ )
                {
                    if(board->state[newX -1 - i][newY -1 + i] != NULL)
                        return false;
                }
            return true;
        }
    }
    return false;
};

Pawn::Pawn(int x_, int y_, int side_, Board *b):
    Piece(x_,y_,side_,b)
{
    adress = "models/pawn.off";
    mesh.loadOFF(adress, 0.10);
};

bool Pawn::moveable(int newX, int newY)
{
    if((newX == x) && (newY == y))
        return false;
    else if(board->state[newX -1][newY - 1] != NULL && type == board->state[newX -1][newY - 1]->type)
        return false;
    //white piece
    else if(type == 1)
    {
        if(y <= 3){
            if((newY - y) == 1 && x == newX)
                return true;
            else return false;
        }
        else
        {
            if((newY - y) == 1)
            {
                if(x == newX && board->state[newX - 1][newY -1] == NULL)
                    return true;
                if(x != newX &&((x - newX) == 1 || (x - newX) == -1))
                    return true;
            }
            else return false;
        }
    }
    // black piece
    else
    {
        if(y >= 6){
            if((newY - y) == -1 && x == newX)
                return true;
            else return false;
        }
        else
        {
            if((newY - y) == -1)
            {
                if(x == newX && board->state[newX - 1][newY -1] == NULL)
                    return true;
                if(x != newX &&((x - newX) == 1 || (x - newX) == -1))
                    return true;
            }
            else return false;
        }
    }
  // to do
    return false;
};
void Board::setPiece(int x, int y, Piece * p)
{
    state[x-1][y-1] = p;
}
